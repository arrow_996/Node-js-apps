const https = require("https");
const http = require("http");
function printError(message){

  console.error(message)

}

function getProfile(username) {
    try {


        const request = https.get("https://teamtreehouse.com/" + username + ".json", response => {

            // console.log(response.statusCode)
          
            if(response.statusCode == 200){
                          let body = ""
                          response.on('data', (d) => {
              
                              body += d.toString();
              
                          });
              
                          response.on('end', () => {
                              
                               try{
                                    const profile = JSON.parse(body);
                                    console.log("User name :", username)
                                    console.log("Badges : ", profile.badges.length);
                                    console.log("Profile Points: ", profile.points.JavaScript);
                                  }
                              catch(error){
                                  
                                    printError(error.message)
                      
                                  }
              
                          });
            }
             
           else{
           
             
              const message = `There was an Error getting the ${username}'s Profile  (${http.STATUS_CODES[response.statusCode]})`
              //const statusCodeError = new Error(message);
             // console.log(statusCodeError)
              printError(message)
           }
             




        });

        request.on('error', (error) => console.error(`Error is ${error}`))

    } catch (error) {

        printError(error.message)

    }



}

module.exports.getProfile = getProfile;