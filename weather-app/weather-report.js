const https = require("https");
const http = require("http");
const key = require("./key");


function printError(message) {

	console.error(message)

}

function weather_report(lat,long) {

	// [33.3601,-25.0589] , [16.3601,-16.0589]
	
	const api_key = key.key;

	try {
		const request = https.get(`https://api.darksky.net/forecast/${api_key}/${lat},${long}`, response => {


			if (response.statusCode == 200) {
				let body = ""
				response.on('data', (d) => {

					body += d.toString();

				});

				response.on('end', () => {

					try {
						const weather_Details = JSON.parse(body);
						let report = `Weather in ${weather_Details.timezone} is ${weather_Details.currently.summary} and temperature is ${weather_Details.currently.temperature}`
						console.log(report);
					} catch (error) {

						printError(error.message)

					}

				});
			} else {


				const message = `There was an Error getting weather Data for location  (${http.STATUS_CODES[response.statusCode]})`
				
				printError(message)
			}



		});

		request.on('error', (error) => console.error(`Error is ${error}`))

	} catch (error) {

		printError(error.message)

	}


}

module.exports = weather_report;